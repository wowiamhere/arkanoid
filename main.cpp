#include <chrono>
#include <stdio.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;

//*************instead of float
using FTime = float;

//**************for window
const unsigned int WW{ 800 }, WH{ 600 };

//*********for ball
const float BRAD{ 10.f },BVEL{ 0.5f };

//*********for floating surface
const float PW{ 60.f }, PH{ 20.f }, PV{ 0.8f };

//*********for bricks (ball to collide with)
const float BW{ 60.f }, BH{ 20.f };
const int BX{ 11 }, BY{ 4 };

//********for frame rate
const float ftStep{ 1.f }, ftSlice{ 1.f };

//*********ball/bullit/bouncing-ball
struct Ball{

//********a circle
	sf::CircleShape shp;

//********* vector with floats of change in coordinates (x,y)
	sf::Vector2f vel{ -BVEL, -BVEL };

//*********constructor, assigning position in window to start
//******************also setting other shape settings
	Ball(float x, float y){
		shp.setPosition(x, y);
		shp.setRadius(BRAD);
		shp.setFillColor(sf::Color::Red);
		shp.setOrigin(BRAD, BRAD);
	}

//*********updating that status of the drawing
	void update(FTime frameTime){ 
		shp.move(vel * frameTime); 

		if (lft() < 0)
			vel.x = BVEL;
		else if (rgh() > WW)
			vel.x = -BVEL;

		if (tp() < 0)
			vel.y = BVEL;
		else if (btm() > WH)
			vel.y = -BVEL;

	}

//*********coordinates of the shape drawn
	float cordX() const { return shp.getPosition().x; }
	float cordY() const { return shp.getPosition().y; }
	float lft() const {	return cordX() - shp.getRadius(); }
	float rgh() const { return cordX() + shp.getRadius(); }
	float tp() const { return cordY() - shp.getRadius(); }
	float btm() const { return cordY() + shp.getRadius(); }
};

//*********base class for Pad and Brick
//*********to help with repeating code
struct GameShape{
	sf::RectangleShape shape;

	float cordX() const { return shape.getPosition().x; }
	float cordY(){ return shape.getPosition().y; }
	float lft(){ return cordX() - shape.getSize().x / 2.f; }
	float rgh(){ return cordX() + shape.getSize().x / 2.f; }
	float tp(){ return cordY() - shape.getSize().y / 2.f; }
	float btm(){ return cordY() + shape.getSize().y / 2.f; }

};

//*********the surface where the ball bounces
//*********controlled by user
struct Pad : public GameShape{
	sf::Vector2f vel;

	Pad(float x, float y){
		shape.setPosition(x, y);
		shape.setSize({ PW, PH });
		shape.setFillColor(sf::Color::Blue);
		shape.setOrigin(PW / 2.f, PH / 2.f);
	}

	void update(FTime frameTime){
		shape.move(vel * frameTime);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left) && lft() > 0 )
				vel.x = -PV;
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right) && rgh() < WW )
				vel.x = PV;
		else
			vel.x = 0;
			
	}
};

//*********for the brinks
struct Brick : public GameShape{
	bool kill{ false };

	Brick(float x, float y){
		shape.setPosition(x, y);
		shape.setSize({ BW, BH });
		shape.setFillColor(sf::Color::Yellow);
		shape.setOrigin(BW / 2.f, BH / 2.f);
	}
};


//*********are two itmes touching?
template <class T1, class T2>
bool inter(T1& a, T2& b){
	return a.rgh() >= b.lft() && a.lft() <= b.rgh() && a.btm() >= b.tp() && a.tp() <= b.btm();
}

//*********apply changes to ball if shapes coliding (ball  & platform)
void col(Pad& p, Ball& b){
	if (!inter(p, b))
		return;

	b.vel.y = -BVEL;

	if (b.cordX() < p.cordX())
		b.vel.x = -BVEL;
	else
		b.vel.x = BVEL;
}

void col(Brick& bk, Ball& b){
	if (!inter(bk, b))
		return;

	bk.kill = true;

	float hitLeft{ b.rgh() - bk.lft() };
	float hitRight{ bk.rgh() - b.lft() };
	float hitTop{ b.btm() - bk.tp() };
	float hitBottom{ bk.btm() - b.tp() };

	bool ballHitLeft(abs(hitLeft) < abs(hitRight));
	bool ballHitTop(abs(hitTop) < abs(hitBottom));

	float overlapX{ ballHitLeft ? hitLeft : hitRight };
	float overlapY{ ballHitTop ? hitTop : hitBottom };

	if (abs(overlapX) < abs(overlapY))
		b.vel.x = ballHitLeft ? -BVEL : BVEL;
	else
		b.vel.y = ballHitTop ? -BVEL : BVEL;

}

struct Game{
	//**********creating window
	sf::RenderWindow window{ { WW, WH }, "first Window" };

	//********for keeping last frame and current time in loop
	FTime lFrameTime{ 0.f }, currTime{ 0.f };

	//*******to control loop
	bool runGame{ false };

	//*********ball and paddle to play with
	Ball ball{ WW / 2, WH / 2 };
	Pad pad{ WW / 2, WH - 50 };

	//***********for bricks on screen for ball to colide with
	vector<Brick> bricks;

	Game(){
		//window.setFramerateLimit(240);
		//window.setFramerateLimit(120);
		window.setFramerateLimit(60);
		//window.setFramerateLimit(30);
		//window.setFramerateLimit(15);

		// for bricks on screen to kill
		for (int x{ 0 }; x < BX; ++x){
			for (int y{ 0 }; y < BY; ++y){
				bricks.emplace_back((x + 1) * (BW + 3) + 22, (y + 2) * (BH + 3));
			}
		}
	}

	void run(){
		runGame = true;

		while (runGame){
			//**********event start time
			auto time1(chrono::high_resolution_clock::now());

			window.clear(sf::Color::Black);

			inputInfo();
			updateGame();
			drawShapes();

			//event end time
			auto time2(chrono::high_resolution_clock::now());

			//keeping track of time for frame rate
			auto timePassed(time2 - time1);

			//Casting to duration type <float, mili>
			//Using count() to return into ft how many miliseconds have passed
			FTime ft{ chrono::duration_cast<chrono::duration<float, milli>>(timePassed).count() };

			//***********assigning new frame time to lFrameTime
			lFrameTime = ft;

			//turning milisecones into seconds
			auto ftSec(ft / 1000.f);
			//getting frame per second
			auto fps(1.f / ftSec);

			window.setTitle("FT: " + to_string(ft) + "\\tFPS: " + to_string(fps));

		}
	}

	void inputInfo(){
		sf::Event event;

		while (window.pollEvent(event)){

			if (event.type == sf::Event::Closed)
				window.close();
		}
	}

	void updateGame(){
		//*********for updating functions everytime currTime surpases
		//*********ftSlice = 1
		currTime = lFrameTime;
		for (; currTime > ftSlice; currTime -= ftSlice){
			ball.update(ftStep);
			pad.update(ftStep);

			//*********call to address collission of itmes
			//*********ball and platform 
			col(pad, ball);

			for (auto& b : bricks)
				col(b, ball);

			bricks.erase(remove_if(begin(bricks), end(bricks), [](const Brick& b){ return b.kill; }), end(bricks));
		}
	}

	void drawShapes(){
		window.draw(ball.shp);
		window.draw(pad.shape);
		for (auto& b : bricks)
			window.draw(b.shape);

		window.display();
	}
};

int main(){
	
	Game{}.run();
	return 0;

}